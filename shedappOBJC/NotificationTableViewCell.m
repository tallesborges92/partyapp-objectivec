//
//  NotificationTableViewCell.m
//  shedappOBJC
//
//  Created by Talles  Borges  on 4/1/15.
//  Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import "NotificationTableViewCell.h"

@implementation NotificationTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (UIEdgeInsets)layoutMargins {
    return UIEdgeInsetsZero;
}
@end
