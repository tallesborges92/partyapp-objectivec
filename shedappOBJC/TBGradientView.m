//
//  TBGradientView.m
//  shedappOBJC
//
//  Created by Talles  Borges  on 5/9/15.
//  Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import "TBGradientView.h"

@implementation TBGradientView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    //// General Declarations
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //// Gradient Declarations
    CGFloat gradientLocations[] = {0, 1};
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef)@[(id)_colorStart.CGColor, (id)_colorEnd.CGColor], gradientLocations);
    
    CGContextSaveGState(context);
    CGContextDrawLinearGradient(context, gradient,
                                CGPointMake(CGRectGetMidX(rect), CGRectGetMinY(rect)),
                                CGPointMake(CGRectGetMidX(rect), CGRectGetMaxY(rect)),
                                0);
    CGContextRestoreGState(context);
    
    
    //// Cleanup
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorSpace);
}


@end
