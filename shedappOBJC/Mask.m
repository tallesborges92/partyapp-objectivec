//
//  Mask.m
//  shedappOBJC
//
//  Created by Talles  Borges  on 5/7/15.
//  Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import "Mask.h"


@implementation Mask

@dynamic maskId;
@dynamic maskImage;
@dynamic maskThumb;

@end
