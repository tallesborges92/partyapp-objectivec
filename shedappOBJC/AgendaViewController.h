//
//  AgendaViewController.h
//  shedapp
//
//  Created by Talles  Borges  on 2/3/15.
//  Copyright (c) 2015 Talles Borges. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RPSlidingMenuViewController.h"

@interface AgendaViewController : RPSlidingMenuViewController <NSFetchedResultsControllerDelegate>

@end
