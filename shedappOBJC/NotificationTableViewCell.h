//
//  NotificationTableViewCell.h
//  shedappOBJC
//
//  Created by Talles  Borges  on 4/1/15.
//  Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *notificationTextLabel;

@end
