//
//  Notification.h
//  shedappOBJC
//
//  Created by Talles  Borges  on 5/7/15.
//  Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Notification : NSManagedObject

@property (nonatomic, retain) NSNumber * notificationId;
@property (nonatomic, retain) NSString * notificationText;
@property (nonatomic, retain) NSDate * notificationDate;

@end
