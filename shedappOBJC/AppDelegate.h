//
//  AppDelegate.h
//  shedappOBJC
//
//  Created by Talles  Borges  on 2/3/15.
//  Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

