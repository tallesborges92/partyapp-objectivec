//
// Created by Talles  Borges  on 5/8/15.
// Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Enterprise.h"


@interface YourClub : NSObject

@property Enterprise *enterprise;

+ (YourClub *)instance;


@end