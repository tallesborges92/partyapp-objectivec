//
//  NotificationViewController.m
//  shedappOBJC
//
//  Created by Talles  Borges  on 4/1/15.
//  Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import <AFNetworking/AFHTTPRequestOperation.h>
#import <RestKit/CoreData/RKManagedObjectStore.h>
#import "RKMappingResult.h"
#import "NotificationViewController.h"
#import "NotificationTableViewCell.h"
#import "NSDate+DateTools.h"
#import "Notification.h"
#import "Notification+RestKit.h"

@interface NotificationViewController ()

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end

@implementation NotificationViewController {
    NSDateFormatter *_df;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIImage *patternImg = [UIImage imageNamed:@"img_pattern"];
//    _containerView.backgroundColor = [UIColor blackColor];
    _backgroundTableView.backgroundColor = [UIColor colorWithPatternImage:patternImg];
    _notificationsTableView.separatorInset = UIEdgeInsetsMake(0, -16, 0, -16);
    _notificationsTableView.tableFooterView = [UIView new];

    _df = [NSDateFormatter new];
    [_df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];

    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;

    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Notification"];
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"notificationId" ascending:NO];
    fetchRequest.sortDescriptors = @[descriptor];
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                        managedObjectContext:[RKManagedObjectStore defaultStore].mainQueueManagedObjectContext
                                                                          sectionNameKeyPath:nil
                                                                                   cacheName:nil];

    [self.fetchedResultsController setDelegate:self];
    [self.fetchedResultsController performFetch:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateNotifications];
}

-(void)updateNotifications {
    [Notification getWithSuccess:^(RKMappingResult *result) {
        NSLog(@"result = %@", result);
    } andFailure:^(NSError *error) {
        NSLog(@"error = %@", error);
    }];
}

#pragma mark - Actions
- (IBAction)closePressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - TableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    NotificationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];

    Notification *notification = [_fetchedResultsController objectAtIndexPath:indexPath];

    cell.notificationTextLabel.text = notification.notificationText;
    cell.dateTimeLabel.text = [notification.notificationDate timeAgoSinceNow];

    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_fetchedResultsController.sections[section] numberOfObjects];
}

#pragma mark - FetchController
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.notificationsTableView reloadData];
}

@end
