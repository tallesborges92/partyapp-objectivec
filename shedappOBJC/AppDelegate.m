//
//  AppDelegate.m
//  shedappOBJC
//
//  Created by Talles  Borges  on 2/3/15.
//  Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import <CoreData/CoreData.h>
#import <RestKit/RestKit.h>
#import "AppDelegate.h"
#import "ContainerViewController.h"
#import "Feed.h"
#import "Feed+RestKit.h"
#import "Enterprise.h"
#import "Enterprise+RestKit.h"
#import "Agenda.h"
#import "Agenda+RestKit.h"
#import "Mask.h"
#import "Mask+RestKit.h"
#import "Notification.h"
#import "Notification+RestKit.h"
#import "Instagram.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    [self initNotification:application];
    [self initRestKit];

    return YES;
}

#pragma mark - Inits
- (void)initNotification:(UIApplication *)application {
    UIUserNotificationType types = UIUserNotificationTypeBadge |
    UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
    // TODO: Compativel com iOS 7
    UIUserNotificationSettings *mySettings =
    [UIUserNotificationSettings settingsForTypes:types categories:nil];

    [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
    [application registerForRemoteNotifications];
}

- (void)initRestKit {
    // URL
    NSURL *baseURL = [NSURL URLWithString:@"http://api.yourclub.mobi"];
    RKObjectManager *objectManager = [RKObjectManager managerWithBaseURL:baseURL];

    // Initialize managed object store
    NSManagedObjectModel *managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    RKManagedObjectStore *managedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];

    // Initialize CoreData Context
    NSString *path = [RKApplicationDataDirectory() stringByAppendingPathComponent:@"Yourclub5.sqlite"];
    [managedObjectStore addSQLitePersistentStoreAtPath:path fromSeedDatabaseAtPath:nil withConfiguration:nil options:nil error:nil];
    [managedObjectStore createManagedObjectContexts];
    managedObjectStore.managedObjectCache = [[RKInMemoryManagedObjectCache alloc] initWithManagedObjectContext:managedObjectStore.persistentStoreManagedObjectContext];

    objectManager.managedObjectStore = managedObjectStore;

    // Models Mapping
    RKResponseDescriptor *feedDescriptor = [Feed responseDescriptorManagedObjectStore:managedObjectStore];
    [objectManager addResponseDescriptor:feedDescriptor];

    RKResponseDescriptor *enterpriseDescriptor = [Enterprise responseDescriptorManagedObjectStore:managedObjectStore];
    [objectManager addResponseDescriptor:enterpriseDescriptor];

    RKResponseDescriptor *agendaDescriptor = [Agenda responseDescriptorManagedObjectStore:managedObjectStore];
    [objectManager addResponseDescriptor:agendaDescriptor];

    RKResponseDescriptor *maskDescriptor = [Mask responseDescriptorManagedObjectStore:managedObjectStore];
    [objectManager addResponseDescriptor:maskDescriptor];

    RKResponseDescriptor *notificationDescriptor = [Notification responseDescriptorManagedObjectStore:managedObjectStore];
    [objectManager addResponseDescriptor:notificationDescriptor];
}

#pragma mark - Notification Delegate Methods
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)devToken {
    const unsigned *tokenBytes = [devToken bytes];
    NSString *hexToken = [NSString stringWithFormat:@"%08x%08x%08x%08x%08x%08x%08x%08x",
                    ntohl(tokenBytes[0]), ntohl(tokenBytes[1]), ntohl(tokenBytes[2]),
                    ntohl(tokenBytes[3]), ntohl(tokenBytes[4]), ntohl(tokenBytes[5]),
                    ntohl(tokenBytes[6]), ntohl(tokenBytes[7])];

    // TODO: Registar Device
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    [manager POST:@"http://partyapp.tallesborges.com/devices.json" parameters:@{@"device[enterpise_id]" : @1, @"device[token]" : hexToken} success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"JSON: %@", responseObject);
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        NSLog(@"Error: %@", error);
//    }];

    NSLog(@"hexToken = %@", hexToken);
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [UIApplication sharedApplication].applicationIconBadgeNumber += 1;
    [[NSNotificationCenter defaultCenter] postNotificationName:kYourClubNotification object:nil];
}

@end
