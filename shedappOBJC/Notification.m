//
//  Notification.m
//  shedappOBJC
//
//  Created by Talles  Borges  on 5/7/15.
//  Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import "Notification.h"


@implementation Notification

@dynamic notificationId;
@dynamic notificationText;
@dynamic notificationDate;

@end
