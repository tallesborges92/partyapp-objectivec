//
//  Agenda.m
//  shedappOBJC
//
//  Created by Talles  Borges  on 5/7/15.
//  Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import "Agenda.h"


@implementation Agenda

@dynamic eventId;
@dynamic eventEndDate;
@dynamic eventStartDate;
@dynamic eventName;
@dynamic eventLongDescription;
@dynamic eventImage;
@dynamic eventShortDescription;

@end
