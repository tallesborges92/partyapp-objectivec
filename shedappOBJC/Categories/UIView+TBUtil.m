//
// Created by Talles  Borges  on 2/11/15.
// Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import "UIView+TBUtil.h"

@implementation UIView (TBUtil)

-(UIImage *) snapshotView {
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.opaque, 0.0f);
    [self drawViewHierarchyInRect:self.bounds afterScreenUpdates:NO];
    UIImage * snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return snapshotImage;
}

@end