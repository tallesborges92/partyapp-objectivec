//
// Created by Talles  Borges  on 5/7/15.
// Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import <RestKit/Network/RKObjectRequestOperation.h>
#import <RestKit/CoreData/RKManagedObjectStore.h>
#import <RestKit/Network/RKObjectManager.h>
#import <RestKit/Network/RKResponseDescriptor.h>
#import "Mask+RestKit.h"
#import "Constants.h"


@implementation Mask (RestKit)

+(RKResponseDescriptor *) responseDescriptorManagedObjectStore:(RKManagedObjectStore *)managedObjectStore {

    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[self entityMappingInManagedObjectStore:managedObjectStore]
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:kMasksUrl
                                                                                           keyPath:nil
                                                                                       statusCodes:[NSIndexSet indexSetWithIndex:200]];

    return responseDescriptor;
}

+(RKEntityMapping *) entityMappingInManagedObjectStore:(RKManagedObjectStore *)managedObjectStore {
    RKEntityMapping *feedMapping = [RKEntityMapping mappingForEntityForName:@"Mask" inManagedObjectStore:managedObjectStore];
    feedMapping.identificationAttributes = @[ @"maskId" ];
    [feedMapping addAttributeMappingsFromDictionary:@{
            @"id" : @"maskId",
            @"image_url" : @"maskImage",
            @"thumb_url" : @"maskThumb",
    }];
    return feedMapping;
}

+(void) getWithSuccess:(void (^)(RKMappingResult *))sucess andFailure:(void (^)(NSError *))failure {
    RKObjectManager *objectManager = [RKObjectManager sharedManager];

    [objectManager getObjectsAtPath:kMasksUrl
                         parameters:nil
                            success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                sucess(mappingResult);
                            }
                            failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                failure(error);
                            }];

}

@end