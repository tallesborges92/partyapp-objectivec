//
// Created by Talles  Borges  on 5/5/15.
// Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import "Enterprise+RestKit.h"
#import "RKObjectManager.h"
#import "Constants.h"


@implementation Enterprise (RestKit)

+ (RKResponseDescriptor *)responseDescriptorManagedObjectStore:(RKManagedObjectStore *)managedObjectStore {
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[self entityMappingInManagedObjectStore:managedObjectStore]
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:kInfoUrl
                                                                                           keyPath:nil
                                                                                       statusCodes:[NSIndexSet indexSetWithIndex:200]];

    return responseDescriptor;
}

+(instancetype) enterpriseWithId:(NSNumber *)enterpriseId {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Enterprise"];

    NSPredicate *predicate =[NSPredicate predicateWithFormat:@"enterpriseId==%@",enterpriseId];

    [fetchRequest setPredicate:predicate];

    NSArray *array = [[RKManagedObjectStore defaultStore].mainQueueManagedObjectContext executeFetchRequest:fetchRequest error:nil];

    Enterprise *enterprise = [array firstObject];

    return enterprise;
}



+(RKEntityMapping *) entityMappingInManagedObjectStore:(RKManagedObjectStore *)managedObjectStore {
    RKEntityMapping *feedMapping = [RKEntityMapping mappingForEntityForName:@"Enterprise" inManagedObjectStore:managedObjectStore];
    feedMapping.identificationAttributes = @[ @"enterpriseId" ];
    [feedMapping addAttributeMappingsFromDictionary:@{
            @"id" : @"enterpriseId",
            @"name" : @"enterpriseName",
            @"description" : @"enterpriseDescription",
            @"telephone" : @"enterpriseTelephone",
            @"addres" : @"enterpriseAddress",
            @"club_image" : @"enterpriseImage",
            @"logo_url" : @"enterpriseLogo",
            @"website" : @"enterpriseWebsite",
            @"hashtag" : @"enterpriseHashtag",

    }];
    return feedMapping;
}

+ (void)getWithSuccess:(void (^)(RKMappingResult *))sucess andFailure:(void (^)(NSError *))failure {
    RKObjectManager *objectManager = [RKObjectManager sharedManager];

    [objectManager getObjectsAtPath:kInfoUrl
                         parameters:nil
                            success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                sucess(mappingResult);
                            }
                            failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                failure(error);
                            }];
}

@end