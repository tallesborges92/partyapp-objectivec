//
// Created by Talles  Borges  on 5/2/15.
// Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Feed.h"
#import "RKEntityMapping.h"
#import "RKResponseDescriptor.h"
#import "RKManagedObjectStore.h"

@interface Feed (RestKit)

+ (RKResponseDescriptor *)responseDescriptorManagedObjectStore:(RKManagedObjectStore *)managedObjectStore;

+ (void)getWithSuccess:(void (^)(RKMappingResult *))sucess andFailure:(void (^)(NSError *))failure;
@end