//
// Created by Talles  Borges  on 5/2/15.
// Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import <RestKit/Network/RKObjectManager.h>
#import "Feed+RestKit.h"
#import "RKResponseDescriptor.h"
#import "RKManagedObjectStore.h"
#import "RKMappingResult.h"
#import "Constants.h"


@implementation Feed (RestKit)

+(RKResponseDescriptor *) responseDescriptorManagedObjectStore:(RKManagedObjectStore *)managedObjectStore {

    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[self entityMappingInManagedObjectStore:managedObjectStore]
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:kFeedUrl
                                                                                           keyPath:@"feed"
                                                                                       statusCodes:[NSIndexSet indexSetWithIndex:200]];

    return responseDescriptor;
}

+(RKEntityMapping *) entityMappingInManagedObjectStore:(RKManagedObjectStore *)managedObjectStore {
    RKEntityMapping *feedMapping = [RKEntityMapping mappingForEntityForName:@"Feed" inManagedObjectStore:managedObjectStore];
    feedMapping.identificationAttributes = @[ @"feedId" ];
    [feedMapping addAttributeMappingsFromDictionary:@{
            @"id" : @"feedId",
            @"day" : @"feedDate",
            @"description" : @"feedDescription",
            @"image_url" : @"feedImageUrl",
    }];
    return feedMapping;
}

+(void) getWithSuccess:(void (^)(RKMappingResult *))sucess andFailure:(void (^)(NSError *))failure {
    RKObjectManager *objectManager = [RKObjectManager sharedManager];

    [objectManager getObjectsAtPath:kFeedUrl
                         parameters:nil
                            success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                sucess(mappingResult);
                            }
                            failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                failure(error);
                            }];

}

@end