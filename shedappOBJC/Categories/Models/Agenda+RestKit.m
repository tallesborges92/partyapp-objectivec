//
// Created by Talles  Borges  on 5/7/15.
// Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import <RestKit/Network/RKObjectRequestOperation.h>
#import <RestKit/CoreData/RKEntityMapping.h>
#import <RestKit/Network/RKResponseDescriptor.h>
#import "Agenda+RestKit.h"
#import "Constants.h"
#import "RKObjectManager.h"


@implementation Agenda (RestKit)

+(RKResponseDescriptor *) responseDescriptorManagedObjectStore:(RKManagedObjectStore *)managedObjectStore {

    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[self entityMappingInManagedObjectStore:managedObjectStore]
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:kAgendaUrl
                                                                                           keyPath:@"agenda"
                                                                                       statusCodes:[NSIndexSet indexSetWithIndex:200]];

    return responseDescriptor;
}

+(RKEntityMapping *) entityMappingInManagedObjectStore:(RKManagedObjectStore *)managedObjectStore {
    RKEntityMapping *feedMapping = [RKEntityMapping mappingForEntityForName:@"Agenda" inManagedObjectStore:managedObjectStore];
    feedMapping.identificationAttributes = @[ @"eventId" ];
    [feedMapping addAttributeMappingsFromDictionary:@{
            @"id" : @"eventId",
            @"start_date" : @"eventStartDate",
            @"end_date" : @"eventEndDate",
            @"event_name" : @"eventName",
            @"description" : @"eventShortDescription",
            @"event_description" : @"eventLongDescription",
            @"image_url" : @"eventImage"
    }];
    return feedMapping;
}

+(void) getWithSuccess:(void (^)(RKMappingResult *))success andFailure:(void (^)(NSError *))failure {
    RKObjectManager *objectManager = [RKObjectManager sharedManager];

    [objectManager getObjectsAtPath:kAgendaUrl
                         parameters:nil
                            success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                success(mappingResult);
                            }
                            failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                failure(error);
                            }];

}

@end