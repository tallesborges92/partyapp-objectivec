//
// Created by Talles  Borges  on 5/5/15.
// Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/Network/RKResponseDescriptor.h>
#import "Enterprise.h"
#import "RKMappingResult.h"
#import "RKManagedObjectStore.h"

@interface Enterprise (RestKit)

+ (RKResponseDescriptor *)responseDescriptorManagedObjectStore:(RKManagedObjectStore *)managedObjectStore;

+ (instancetype)enterpriseWithId:(NSNumber *)enterpriseId;

+ (void)getWithSuccess:(void (^)(RKMappingResult *))sucess andFailure:(void (^)(NSError *))failure;

@end