//
// Created by Talles  Borges  on 5/7/15.
// Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RKMappingResult.h"
#import "RKResponseDescriptor.h"
#import "RKManagedObjectStore.h"
#import "Mask.h"

@interface Mask (RestKit)
+ (RKResponseDescriptor *)responseDescriptorManagedObjectStore:(RKManagedObjectStore *)managedObjectStore;

+ (void)getWithSuccess:(void (^)(RKMappingResult *))sucess andFailure:(void (^)(NSError *))failure;
@end