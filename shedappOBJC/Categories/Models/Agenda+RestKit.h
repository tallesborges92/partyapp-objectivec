//
// Created by Talles  Borges  on 5/7/15.
// Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RKMappingResult.h"
#import "RKResponseDescriptor.h"
#import "RKManagedObjectStore.h"
#import "Agenda.h"

@interface Agenda (RestKit)

+ (RKResponseDescriptor *)responseDescriptorManagedObjectStore:(RKManagedObjectStore *)managedObjectStore;

+ (void)getWithSuccess:(void (^)(RKMappingResult *))success andFailure:(void (^)(NSError *))failure;
@end