//
// Created by Talles  Borges  on 5/7/15.
// Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import <RestKit/Network/RKObjectRequestOperation.h>
#import <RestKit/Network/RKObjectManager.h>
#import <RestKit/CoreData/RKEntityMapping.h>
#import <RestKit/Network/RKResponseDescriptor.h>
#import "Notification+RestKit.h"
#import "Constants.h"


@implementation Notification (RestKit)

+(RKResponseDescriptor *) responseDescriptorManagedObjectStore:(RKManagedObjectStore *)managedObjectStore {

    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:[self entityMappingInManagedObjectStore:managedObjectStore]
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:kNotificationsUrl
                                                                                           keyPath:@"notifications"
                                                                                       statusCodes:[NSIndexSet indexSetWithIndex:200]];

    return responseDescriptor;
}

+(RKEntityMapping *) entityMappingInManagedObjectStore:(RKManagedObjectStore *)managedObjectStore {
    RKEntityMapping *feedMapping = [RKEntityMapping mappingForEntityForName:@"Notification" inManagedObjectStore:managedObjectStore];
    feedMapping.identificationAttributes = @[ @"notificationId" ];
    [feedMapping addAttributeMappingsFromDictionary:@{
            @"id" : @"notificationId",
            @"text" : @"notificationText",
            @"date" : @"notificationDate",
    }];
    return feedMapping;
}

+(void) getWithSuccess:(void (^)(RKMappingResult *))sucess andFailure:(void (^)(NSError *))failure {
    RKObjectManager *objectManager = [RKObjectManager sharedManager];

    [objectManager getObjectsAtPath:kNotificationsUrl
                         parameters:nil
                            success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                                sucess(mappingResult);
                            }
                            failure:^(RKObjectRequestOperation *operation, NSError *error) {
                                failure(error);
                            }];

}

@end