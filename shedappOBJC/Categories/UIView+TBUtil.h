//
// Created by Talles  Borges  on 2/11/15.
// Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface UIView (TBUtil)
- (UIImage *)snapshotView;
@end