//
// Created by Talles  Borges  on 5/7/15.
// Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import <Foundation/Foundation.h>

@class InstagramResult;

@interface Instagram : NSObject

+ (void)getWithTag:(NSString *)tag maxId:(NSString *)maxId Success:(void (^)(InstagramResult *))success andFailure:(void (^)(NSError *))failure;

@end

@interface InstaItem : NSObject

@property NSString *lowResolution;
@property NSString *thumbResolution;
@property NSString *standardResolution;

@end


@interface InstagramResult : NSObject

@property NSString *nextMaxId;
@property NSArray *instaImages;

@end