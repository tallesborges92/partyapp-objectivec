//
//  Feed.m
//  shedappOBJC
//
//  Created by Talles  Borges  on 5/2/15.
//  Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import "Feed.h"


@implementation Feed

@dynamic feedId;
@dynamic feedDescription;
@dynamic feedDate;
@dynamic feedImageUrl;

@end
