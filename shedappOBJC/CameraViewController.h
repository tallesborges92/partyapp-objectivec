//
//  CameraViewController.h
//  shedappOBJC
//
//  Created by Talles  Borges  on 2/4/15.
//  Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCRecorder.h"

@interface CameraViewController : UIViewController <UICollectionViewDataSource,UICollectionViewDelegate, SCRecorderDelegate, UIDocumentInteractionControllerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, NSFetchedResultsControllerDelegate>

#pragma mark - Models


#pragma mark - Outlets
@property (weak, nonatomic) IBOutlet UICollectionView *masksCollectionView;
@property (weak, nonatomic) IBOutlet UIView *cameraView;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIButton *takePhotoButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;


#pragma mark - Actions

- (IBAction)closePressed:(id)sender;
- (IBAction)uploadPressed:(id)sender;
- (IBAction)cameraPressed:(id)sender;
- (IBAction)flashPressed:(id)sender;
- (IBAction)photoPressed:(id)sender;
- (IBAction)addPhotoPressed:(id)sender;
- (IBAction)deletePressed:(id)sender;


@end
