//
//  NotificationViewController.h
//  shedappOBJC
//
//  Created by Talles  Borges  on 4/1/15.
//  Copyright (c) 2015 TallesBorges. All rights reserved.
//

@import CoreData;
#import <UIKit/UIKit.h>

@interface NotificationViewController : UIViewController <UITableViewDataSource, NSFetchedResultsControllerDelegate>

#pragma mark - Outlet
@property (weak, nonatomic) IBOutlet UITableView *notificationsTableView;
@property (weak, nonatomic) IBOutlet UIView *backgroundTableView;

#pragma mark - Actions
- (IBAction)closePressed:(id)sender;

@end
