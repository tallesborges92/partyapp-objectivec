//
//  InstagramViewController.m
//  shedapp
//
//  Created by Talles  Borges  on 2/3/15.
//  Copyright (c) 2015 Talles Borges. All rights reserved.
//

#import "InstagramViewController.h"
#import "InstagramCollectionViewCell.h"
#import "UIImageView+WebCache.h"
#import "Instagram.h"
#import "YourClub.h"

@interface  InstagramViewController ()


@end

@implementation InstagramViewController {
    NSMutableArray *_photos;
    bool _loading;
    NSString *_maxId;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    FMMosaicLayout *mosaicLayout = [[FMMosaicLayout alloc] init];
    mosaicLayout.delegate = self;
    self.photosCollectionView.collectionViewLayout = mosaicLayout;

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    _maxId = nil;
    _photos = [NSMutableArray array];
    [self loadPhotos];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    _photos = [NSMutableArray array];
    [_photosCollectionView reloadData];
}


-(void) loadPhotos {
    _loading = true;
    [Instagram getWithTag:[YourClub instance].enterprise.enterpriseHashtag maxId:_maxId Success:^(InstagramResult *result) {
        _maxId = result.nextMaxId;
        [_photos addObjectsFromArray:result.instaImages];
        [_photosCollectionView reloadData];
        _loading = false;
    } andFailure:^(NSError *error) {
        _loading = false;
        NSLog(@"error = %@", error);
    }];
}

#pragma mark - CollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _photos.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    InstagramCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];

    if(indexPath.row > _photos.count) return cell;

    InstaItem *item = _photos[indexPath.row];

    [cell.photoImageView sd_setImageWithURL:[NSURL URLWithString:item.thumbResolution] placeholderImage:[UIImage imageNamed:@"InstaFail"]] ;
    [cell.photoImageView setupImageViewerWithImageURL:[NSURL URLWithString:item.standardResolution]];
    [cell.photoImageView setupImageViewerWithDatasource:self initialIndex:indexPath.row onOpen:nil onClose:nil];

    return cell;
}

#pragma mark - MHInstagram

- (NSInteger)numberImagesForImageViewer:(MHFacebookImageViewer *)imageViewer {
    return _photos.count;
}

- (NSURL *)imageURLAtIndex:(NSInteger)index imageViewer:(MHFacebookImageViewer *)imageViewer {
    InstaItem *item = _photos[index];
    return [NSURL URLWithString:item.standardResolution];
}

- (UIImage *)imageDefaultAtIndex:(NSInteger)index imageViewer:(MHFacebookImageViewer *)imageViewer {
    return [UIImage imageNamed:@"carregando"];
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == _photos.count - 1) {
        if(!_loading && _maxId) {
            [self loadPhotos];
        }
    }

}

#pragma mark - FMMosaic

- (NSInteger)collectionView:(UICollectionView *)collectionView layout:(FMMosaicLayout *)collectionViewLayout numberOfColumnsInSection:(NSInteger)section {
    return 2;
}

- (FMMosaicCellSize)collectionView:(UICollectionView *)collectionView layout:(FMMosaicLayout *)collectionViewLayout mosaicCellSizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return (indexPath.item % 12 == 0) ? FMMosaicCellSizeBig : FMMosaicCellSizeSmall;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(FMMosaicLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(44, 0, 44, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(FMMosaicLayout *)collectionViewLayout interitemSpacingForSectionAtIndex:(NSInteger)section {
    return 2.0;
}

@end
