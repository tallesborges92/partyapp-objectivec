//
//  InstagramViewController.h
//  shedapp
//
//  Created by Talles  Borges  on 2/3/15.
//  Copyright (c) 2015 Talles Borges. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MHFacebookImageViewer.h"
#import "FMMosaicLayout.h"

@interface InstagramViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate, UIScrollViewDelegate, MHFacebookImageViewerDatasource, FMMosaicLayoutDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *photosCollectionView;

@end
