//
//  InstagramCollectionViewCell.m
//  shedappOBJC
//
//  Created by Talles  Borges  on 2/4/15.
//  Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import "InstagramCollectionViewCell.h"

@implementation InstagramCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];

    [_photoImageView.layer setBorderColor:[UIColor colorWithRed:0.96 green:0.93 blue:0.85 alpha:1].CGColor];
    [_photoImageView.layer setBorderWidth:1];
}


@end
