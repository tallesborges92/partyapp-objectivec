//
//  Enterprise.h
//  shedappOBJC
//
//  Created by Talles  Borges  on 5/5/15.
//  Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Enterprise : NSManagedObject

@property (nonatomic, retain) NSString * enterpriseName;
@property (nonatomic, retain) NSString * enterpriseAddress;
@property (nonatomic, retain) NSString * enterpriseTelephone;
@property (nonatomic, retain) NSString * enterpriseDescription;
@property (nonatomic, retain) NSString * enterpriseWebsite;
@property (nonatomic, retain) NSString * enterpriseImage;
@property (nonatomic, retain) NSString * enterpriseLatitude;
@property (nonatomic, retain) NSNumber * enterpriseId;
@property (nonatomic, retain) NSString * enterpriseLongitude;
@property (nonatomic, retain) NSString * enterpriseLogo;
@property (nonatomic, retain) NSString * enterpriseHashtag;

@end
