//
//  AgendaViewController.m
//  shedapp
//
//  Created by Talles  Borges  on 2/3/15.
//  Copyright (c) 2015 Talles Borges. All rights reserved.
//

#import "Agenda+RestKit.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AgendaViewController.h"
#import "AgendaDetailViewController.h"

@interface AgendaViewController ()

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end

@implementation AgendaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Agenda"];
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"eventStartDate" ascending:YES];
    fetchRequest.sortDescriptors = @[descriptor];
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                        managedObjectContext:[RKManagedObjectStore defaultStore].mainQueueManagedObjectContext
                                                                          sectionNameKeyPath:nil
                                                                                   cacheName:nil];
    [self.fetchedResultsController setDelegate:self];
    [self.fetchedResultsController performFetch:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateAgenda];
}

-(void) updateAgenda {
    [Agenda getWithSuccess:^(RKMappingResult *result) {
        NSLog(@"result = %@", result);
    } andFailure:^(NSError *error) {
        NSLog(@"error = %@", error);
    }];

}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [super collectionView:collectionView didSelectItemAtIndexPath:indexPath];

    AgendaDetailViewController *detailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"agendaDetailVC"];
    detailVC.agenda = [_fetchedResultsController objectAtIndexPath:indexPath];

    [self presentViewController:detailVC animated:YES completion:nil];
}

- (NSInteger)numberOfItemsInSlidingMenu {
    id<NSFetchedResultsSectionInfo> sectionInfo = self.fetchedResultsController.sections[0];
    return sectionInfo.numberOfObjects;
}

- (void)customizeCell:(RPSlidingMenuCell *)slidingMenuCell forRow:(NSInteger)row {
    Agenda *agenda = [_fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:row inSection:0]];

    [slidingMenuCell.backgroundImageView sd_setImageWithURL:[NSURL URLWithString:agenda.eventImage] placeholderImage:[UIImage imageNamed:@"Placeholder"]];
    [slidingMenuCell.detailTextLabel setText:agenda.eventShortDescription];

    NSDate *date = agenda.eventStartDate;

    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:@"EE, dd/MM"];

    NSString *stringFromDate = [df stringFromDate:date];

    stringFromDate = [stringFromDate stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:[[stringFromDate substringToIndex:1] capitalizedString]];

    [slidingMenuCell.textLabel setText:stringFromDate];
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.collectionView reloadData];
}
@end
