//
//  ContainerViewController.m
//  shedapp
//
//  Created by Talles  Borges  on 2/3/15.
//  Copyright (c) 2015 Talles Borges. All rights reserved.
//

#import "ContainerViewController.h"
#import "NotificationViewController.h"
#import "JSBadgeView.h"
#import "Enterprise.h"
#import "Enterprise+RestKit.h"
#import "YourClub.h"

@interface ContainerViewController ()

@end

@implementation ContainerViewController {
    UIViewController *_feed;
    UIViewController *_agenda;
    UIViewController *_instagram;
    UIViewController *_about;
    UIViewController *_selectedVC;
    UIImage *_feedON;
    UIImage *_agendaON;
    UIImage *_cameraON;
    UIImage *_instaON;
    UIImage *_infoON;
    UIImage *_feedOFF;
    UIImage *_agendaOFF;
    UIImage *_cameraOFF;
    UIImage *_instaOFF;
    UIImage *_infoOFF;
    JSBadgeView *_badgeView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [Enterprise getWithSuccess:^(RKMappingResult *result) {
        [YourClub instance].enterprise = [result firstObject];
    } andFailure:^(NSError *error) {
        NSLog(@"error = %@", error);
    }];

    _feed = [self.storyboard instantiateViewControllerWithIdentifier:@"feedVC"];
    _agenda = [self.storyboard instantiateViewControllerWithIdentifier:@"agendaVC"];
    _instagram = [self.storyboard instantiateViewControllerWithIdentifier:@"instagramVC"];
    _about = [self.storyboard instantiateViewControllerWithIdentifier:@"aboutVC"];

    [self addChildViewController:_feed];
    [self addChildViewController:_agenda];
    [self addChildViewController:_instagram];
    [self addChildViewController:_about];

    _feedON = [UIImage imageNamed:@"tabbar_ic_noticias_ON"];
    _agendaON = [UIImage imageNamed:@"tabbar_ic_agenda_ON"];
    _cameraON = [UIImage imageNamed:@"ic_camera"];
    _instaON = [UIImage imageNamed:@"tabbar_ic_galeria_ON"];
    _infoON = [UIImage imageNamed:@"tabbar_ic_info_ON"];

    _feedOFF = [UIImage imageNamed:@"ic_feed"];
    _agendaOFF = [UIImage imageNamed:@"ic_agenda"];
    _cameraOFF = [UIImage imageNamed:@"ic_camera"];
    _instaOFF = [UIImage imageNamed:@"ic_instagram"];
    _infoOFF = [UIImage imageNamed:@"ic_about"];

    _selectedVC = _feed;
    [_feedButton setImage:_feedON forState:UIControlStateNormal];
    [_containerView addSubview:_feed.view];
    UIImage *patternImg = [UIImage imageNamed:@"img_pattern"];
    _containerView.backgroundColor = [UIColor colorWithPatternImage:patternImg];

    _badgeView = [[JSBadgeView alloc] initWithParentView:_notificationButton alignment:JSBadgeViewAlignmentTopRight];
    _badgeView.badgePositionAdjustment = CGPointMake(-10, 10);

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateBadge) name:kYourClubNotification object:nil];
}

- (void)updateBadge {
    NSInteger badgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber;
    if (badgeNumber > 0) {
    _badgeView.badgeText = [NSString stringWithFormat:@"%d", badgeNumber];
    } else {
        _badgeView.badgeText = @"";
    }

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateBadge];
}


-(void) allButtonsOff {
    [_feedButton setImage:_feedOFF forState:UIControlStateNormal];
    [_agendaButton setImage:_agendaOFF forState:UIControlStateNormal];
    [_instaButton setImage:_instaOFF forState:UIControlStateNormal];
    [_infoButton setImage:_infoOFF forState:UIControlStateNormal];
}

- (IBAction)feedPressed:(id)sender {
    _titleLabel.text = @"Feed";
    [self addVC:_feed];
    [self allButtonsOff];
    [_feedButton setImage:_feedON forState:UIControlStateNormal];
}


- (IBAction)agendaPressed:(id)sender {
    _titleLabel.text = @"Agenda";
    [self addVC:_agenda];
    [self allButtonsOff];
    [_agendaButton setImage:_agendaON forState:UIControlStateNormal];

}

- (IBAction)cameraPressed:(id)sender {
    UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"cameraVC"];
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)instagramPressed:(id)sender {
    _titleLabel.text = [YourClub instance].enterprise.enterpriseHashtag;
    [self addVC:_instagram];
    [self allButtonsOff];
    [_instaButton setImage:_instaON forState:UIControlStateNormal];

}
- (IBAction)aboutPressed:(id)sender {
    _titleLabel.text = [YourClub instance].enterprise.enterpriseName;
    [self addVC:_about];
    [self allButtonsOff];
    [_infoButton setImage:_infoON forState:UIControlStateNormal];

}
- (IBAction)notificationPressed:(id)sender {
    NotificationViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"notificationVC"];
    [self presentViewController:vc animated:YES completion:nil];
}

-(void) addVC:(UIViewController *)vc {
    [_selectedVC.view removeFromSuperview];
    _selectedVC = vc;
    [_containerView addSubview:vc.view];
    [self allButtonsOff];
}

@end
