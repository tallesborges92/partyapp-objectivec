//
//  CameraViewController.m
//  shedappOBJC
//
//  Created by Talles  Borges  on 2/4/15.
//  Copyright (c) 2015 TallesBorges. All rights reserved.
//

//#import <Social/Social.h>
#import <CoreData/CoreData.h>
#import "CameraViewController.h"
#import "MaskCollectionViewCell.h"
#import "SCRecorder.h"
#import "UIImage+BFKit.h"
#import "UIImage+Utilities.h"
#import "UIImageView+AFNetworking.h"
#import "UIImageView+WebCache.h"
#import "Mask.h"
#import "Mask+RestKit.h"

@interface CameraViewController ()

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end

@implementation CameraViewController {
    SCRecorder *_scRecorder;
    UIImageView *_photoImage;
    UIImageView *_layerImage;
    UIDocumentInteractionController *_docController;
}

#pragma mark - UIView Methods
- (void)viewDidLoad {
    [super viewDidLoad];

    UIImage *patternImg = [UIImage imageNamed:@"img_pattern"];
    self.view.backgroundColor = [UIColor colorWithPatternImage:patternImg];

    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Mask"];
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"maskId" ascending:NO];
    fetchRequest.sortDescriptors = @[descriptor];
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                        managedObjectContext:[RKManagedObjectStore defaultStore].mainQueueManagedObjectContext
                                                                          sectionNameKeyPath:nil
                                                                                   cacheName:nil];

    [self.fetchedResultsController setDelegate:self];
    [self.fetchedResultsController performFetch:nil];

    _scRecorder = [SCRecorder recorder];
    _scRecorder.flashMode = SCFlashModeOff;

    SCAudioConfiguration *audioConfiguration = _scRecorder.audioConfiguration;
    audioConfiguration.enabled = NO;

    _scRecorder.previewView = _cameraView;
    _scRecorder.delegate = self;
    _scRecorder.sessionPreset = [SCRecorderTools bestSessionPresetCompatibleWithAllDevices];
    _scRecorder.autoSetVideoOrientation = YES;

    [_scRecorder openSession: ^(NSError *sessionError, NSError *audioError, NSError *videoError, NSError *photoError) {
        // Start the flow of inputs
        [_scRecorder startRunningSession];
    }];


    [_cancelButton setHidden:YES];
    [_shareButton setHidden:YES];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateMasks];
}


-(void) updateMasks {
    [Mask getWithSuccess:^(RKMappingResult *result) {
        NSLog(@"result = %@", result);
    } andFailure:^(NSError *error) {
        NSLog(@"error = %@", error);
    }];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [_scRecorder previewViewFrameChanged];
}

#pragma mark - Actions

- (IBAction)closePressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)uploadPressed:(id)sender {
    UIImage *combinedImage;
    if (_layerImage) {
        combinedImage = [_photoImage.image mergeWithImage:_layerImage.image];
    }else {
        combinedImage = _photoImage.image;
    }

    NSString *documentDirectory=[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"];
    NSString *saveImagePath=[documentDirectory stringByAppendingPathComponent:@"Image.ig"];

    NSData *imageData = UIImageJPEGRepresentation(combinedImage, 0.8);

    [imageData writeToFile:saveImagePath atomically:YES];

    NSURL *imageURL=[NSURL fileURLWithPath:saveImagePath];

    _docController = [UIDocumentInteractionController interactionControllerWithURL:imageURL];
    _docController.delegate = self;
    _docController.UTI = @"com.instagram.photo";

    _docController.annotation= @{@"InstagramCaption" : @"#shedCuritiba"};


    [_docController presentOptionsMenuFromRect:CGRectZero inView:self.view animated:YES];
}

- (IBAction)cameraPressed:(id)sender {
    [_scRecorder switchCaptureDevices];
}

- (IBAction)flashPressed:(id)sender {
    if (_scRecorder.flashMode == SCFlashModeOff) {
        _scRecorder.flashMode = SCFlashModeOn;
    } else {
        _scRecorder.flashMode = SCFlashModeOff;
    }
}

- (IBAction)photoPressed:(id)sender {

    [_scRecorder capturePhoto:^(NSError *error, UIImage *image) {
        [_scRecorder closeSession];
        if (image) {

            ///
            CGSize imageSize = image.size;
            CGFloat width = imageSize.width;
            CGFloat height = imageSize.height;
            CGRect cropRect;
            if (height > width) {
                cropRect = CGRectMake((height - width) / 2.0f, 0.0f, width, width);
            } else {
                cropRect = CGRectMake((width - height) / 2.0f, 0.0f, width, width);
            }

            UIImage *croppedImage = [image imageAtRect:cropRect];

            if (_scRecorder.device == AVCaptureDevicePositionBack) {
                croppedImage = [UIImage imageWithCGImage:croppedImage.CGImage scale:croppedImage.scale orientation:image.imageOrientation];
            } else {
                croppedImage = [UIImage imageWithCGImage:croppedImage.CGImage scale:croppedImage.scale orientation:UIImageOrientationLeftMirrored];
            }

            [self initPhotoImageWithImage:croppedImage];
        }
    }];
}

-(void)initPhotoImageWithImage:(UIImage *)image {
    if (_photoImage) {
        [_photoImage removeFromSuperview];
    }
    _photoImage = [[UIImageView alloc] initWithImage:image];

    CGSize size = _cameraView.frame.size;

    [_photoImage setFrame:CGRectMake(0, 0, size.width, size.height)];

    [_cameraView insertSubview:_photoImage atIndex:0];

    [_cancelButton setHidden:NO];
    [_shareButton setHidden:NO];

    [_takePhotoButton setHidden:YES];
}

- (IBAction)addPhotoPressed:(id)sender {
    UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
    pickerController.delegate = self;
    pickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    pickerController.allowsEditing = YES;
    [self presentViewController:pickerController animated:YES completion:nil];
}

- (IBAction)deletePressed:(id)sender {
    [_takePhotoButton setHidden:NO];
    [_cancelButton setHidden:YES];
    [_shareButton setHidden:YES];

    [_scRecorder openSession: ^(NSError *sessionError, NSError *audioError, NSError *videoError, NSError *photoError) {
        // Start the flow of inputs
        [_scRecorder startRunningSession];
    }];
}

#pragma mark - PickerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo {
    [_scRecorder closeSession];

    CGRect cropRect = [editingInfo[UIImagePickerControllerCropRect] CGRectValue];

    UIImage *imageOriginal = editingInfo[UIImagePickerControllerOriginalImage];

    UIImage *imageCropped = [imageOriginal croppedImage:cropRect];

    [self initPhotoImageWithImage:imageCropped];

    [picker dismissViewControllerAnimated:YES completion:^{
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:^{
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    }];
}


#pragma mark - Mask Collection DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [_fetchedResultsController.sections[0] numberOfObjects];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    Mask *mask = [_fetchedResultsController objectAtIndexPath:indexPath];

    MaskCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];

    [cell.maskImage sd_setImageWithURL:[NSURL URLWithString:mask.maskThumb]];

    return cell;
}


#pragma mark - Mask Collection Delgate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    Mask *mask = [_fetchedResultsController objectAtIndexPath:indexPath];

    if (_layerImage) {
        [_layerImage removeFromSuperview];
    }

    NSString *imageString = mask.maskImage;

    _layerImage = [[UIImageView alloc] init];
    [_layerImage sd_setImageWithURL:[NSURL URLWithString:imageString]];

    CGSize size = _cameraView.frame.size;

    [_layerImage setFrame:CGRectMake(0, 0, size.width, size.height)];

    [_cameraView addSubview:_layerImage];
}



#pragma mark - DocumentInteractionController
- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller {
    return self;
}


#pragma mark - FetchDelegate
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.masksCollectionView reloadData];
}

@end
