//
//  FeedViewController.m
//  shedapp
//
//  Created by Talles  Borges  on 2/3/15.
//  Copyright (c) 2015 Talles Borges. All rights reserved.
//

#import <SDWebImage/UIImageView+WebCache.h>
#import <CoreData/CoreData.h>
#import "FeedViewController.h"
#import "FeedTableViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "ALView+PureLayout.h"
#import "RKObjectRequestOperation.h"
#import "Feed.h"
#import "Feed+RestKit.h"

static const CGFloat kPadding = 10;
static const CGFloat kImageHeight = 130;

@interface FeedViewController ()

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;

@end

@implementation FeedViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    _feedTableView.contentInset = UIEdgeInsetsMake(44, 0, 44, 0);

    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Feed"];
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"feedDate" ascending:NO];
    fetchRequest.sortDescriptors = @[descriptor];
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                        managedObjectContext:[RKManagedObjectStore defaultStore].mainQueueManagedObjectContext
                                                                          sectionNameKeyPath:nil
                                                                                   cacheName:nil];

    [self.fetchedResultsController setDelegate:self];
    [self.fetchedResultsController performFetch:nil];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self updateFeed];
}


- (void)updateFeed {
    [self.refreshControl beginRefreshing];
    [Feed getWithSuccess:^(RKMappingResult *result) {
        NSLog(@"result = %@", result);
    } andFailure:^(NSError *error) {
        NSLog(@"error = %@", error);
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id<NSFetchedResultsSectionInfo> sectionInfo = self.fetchedResultsController.sections[section];
    return sectionInfo.numberOfObjects;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    Feed *feed = [self.fetchedResultsController objectAtIndexPath:indexPath];

    static NSString *CellIdentifier = @"CellIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor whiteColor];

        JMEParallaxView *parallaxView = [JMEParallaxView new];
        parallaxView.tag = 111;
        parallaxView.direction = JMEParallaxVertical;
        parallaxView.reversedContentRevealing = YES;
        parallaxView.contentDisplayingPercentage = 0.6;
        parallaxView.anchorOffset = tableView.contentInset.top;
        parallaxView.observingScrollView = tableView;

        UILabel *labelText = [UILabel new];
        [labelText setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:13]];
        labelText.numberOfLines = 0;
        labelText.tag = 222;
        labelText.text = @"Mother Fucker !";

        [cell.contentView addSubview:parallaxView];

        [parallaxView autoPinEdge:ALEdgeTop toEdge:ALEdgeTop ofView:cell.contentView withOffset:0];
        [parallaxView autoPinEdge:ALEdgeLeft toEdge:ALEdgeLeft ofView:cell.contentView withOffset:0];
        [parallaxView autoPinEdge:ALEdgeRight toEdge:ALEdgeRight ofView:cell.contentView withOffset:0];
        [parallaxView autoSetDimension:ALDimensionHeight toSize:195];

        [cell.contentView addSubview:labelText];

        [labelText autoPinEdgeToSuperviewEdge:ALEdgeLeading withInset:15];
        [labelText autoPinEdgeToSuperviewEdge:ALEdgeTrailing withInset:15];
        [labelText autoPinEdge:ALEdgeTop toEdge:ALEdgeBottom ofView:parallaxView withOffset:15];

        [cell layoutIfNeeded];
    }


    UILabel *labelText = (UILabel *) [cell viewWithTag:222];
    labelText.text = feed.feedDescription;

    JMEParallaxView *parallaxView = (JMEParallaxView *)[cell viewWithTag:111];
    [parallaxView.contentImageView sd_setImageWithURL:[NSURL URLWithString:feed.feedImageUrl] placeholderImage:[UIImage imageNamed:@"Placeholder"]];
    parallaxView.activeRange = [JMEParallaxView activeRangeWithTableView:tableView
                                                               indexPath:indexPath
                                                               direction:parallaxView.direction
                                                              edgeInsets:UIEdgeInsetsMake(kPadding, 0, kPadding, 0)];

    return cell;
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.feedTableView reloadData];
}

@end
