//
// Created by Talles  Borges  on 5/8/15.
// Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import "YourClub.h"


@implementation YourClub {

}
+ (YourClub *)instance {
    static YourClub *_instance = nil;

    @synchronized (self) {
        if (_instance == nil) {
            _instance = [[self alloc] init];
        }
    }

    return _instance;
}
@end