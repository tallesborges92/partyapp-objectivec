//
//  Agenda.h
//  shedappOBJC
//
//  Created by Talles  Borges  on 5/7/15.
//  Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Agenda : NSManagedObject

@property (nonatomic, retain) NSNumber * eventId;
@property (nonatomic, retain) NSDate * eventEndDate;
@property (nonatomic, retain) NSDate * eventStartDate;
@property (nonatomic, retain) NSString * eventName;
@property (nonatomic, retain) NSString * eventLongDescription;
@property (nonatomic, retain) NSString * eventImage;
@property (nonatomic, retain) NSString * eventShortDescription;

@end
