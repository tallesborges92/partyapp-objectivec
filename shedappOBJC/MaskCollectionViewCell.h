//
//  MaskCollectionViewCell.h
//  shedappOBJC
//
//  Created by Talles  Borges  on 2/4/15.
//  Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MaskCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *maskImage;

@end
