//
// Created by Talles  Borges  on 5/7/15.
// Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import <RestKit/ObjectMapping/RKHTTPUtilities.h>
#import <RestKit/Network/RKResponseDescriptor.h>
#import <RestKit/ObjectMapping/RKRelationshipMapping.h>
#import "Instagram.h"
#import <RestKit/ObjectMapping/RKObjectMapping.h>
#import <RestKit/ObjectMapping/RKAttributeMapping.h>
#import <RestKit/Network/RKObjectManager.h>


@interface Instagram ()

@property RKObjectManager *objectManager;

@end

@implementation Instagram

+ (Instagram *)instance {
    static Instagram *_instance = nil;

    @synchronized (self) {
        if (_instance == nil) {
            _instance = [[self alloc] init];
        }
    }

    return _instance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        NSURL *baseURL = [NSURL URLWithString:@"https://api.instagram.com"];
        _objectManager = [RKObjectManager managerWithBaseURL:baseURL];

        [self initResponses];
    }

    return self;
}

- (void) initResponses {

    RKObjectMapping *instaItem = [RKObjectMapping mappingForClass:[InstaItem class]];
    [instaItem addAttributeMappingsFromDictionary:@{@"images.low_resolution.url" : @"lowResolution"}];
    [instaItem addAttributeMappingsFromDictionary:@{@"images.thumbnail.url" : @"thumbResolution"}];
    [instaItem addAttributeMappingsFromDictionary:@{@"images.standard_resolution.url" : @"standardResolution"}];


    RKObjectMapping *instaResult = [RKObjectMapping mappingForClass:[InstagramResult class]];
    [instaResult addPropertyMapping:[RKAttributeMapping attributeMappingFromKeyPath:@"pagination.next_max_tag_id" toKeyPath:@"nextMaxId"]];
    [instaResult addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"data" toKeyPath:@"instaImages" withMapping:instaItem]];

    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:instaResult
                                                                                            method:RKRequestMethodGET
                                                                                       pathPattern:@"/v1/tags/:tag/media/recent"
                                                                                           keyPath:nil
                                                                                       statusCodes:[NSIndexSet indexSetWithIndex:200]];

    [_objectManager addResponseDescriptor:responseDescriptor];

}

+(void) getWithTag:(NSString *)tag maxId:(NSString *)maxId Success:(void (^)(InstagramResult *))success andFailure:(void (^)(NSError *))failure {

    NSDictionary *params = @{@"client_id" : @"f98b6051b39e4d619c4fd383eb20e5bb"};
    if (maxId) {
        params = @{@"client_id" : @"f98b6051b39e4d619c4fd383eb20e5bb", @"max_tag_id" : maxId};
    }

    NSString *path = [NSString stringWithFormat:@"/v1/tags/%@/media/recent",tag];
    [[self instance].objectManager getObjectsAtPath:path
                                         parameters:params success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                success([mappingResult firstObject]);
            }                               failure:^(RKObjectRequestOperation *operation, NSError *error) {
                failure(error);
            }];
}

@end

@implementation InstaItem{

}
@end

@implementation InstagramResult {

}

@end