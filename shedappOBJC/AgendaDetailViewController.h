//
//  AgendaViewController.h
//  shedapp
//
//  Created by Talles  Borges  on 2/3/15.
//  Copyright (c) 2015 Talles Borges. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Agenda.h"

@interface AgendaDetailViewController : UIViewController <UIScrollViewDelegate>

#pragma mark - Model
@property (weak, nonatomic) Agenda *agenda;

#pragma mark - Outlets
@property (weak, nonatomic) IBOutlet UIImageView *agendaImageView;
@property (weak, nonatomic) IBOutlet UILabel *agendaNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *agendaDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *agendaHourLabel;
@property (weak, nonatomic) IBOutlet UILabel *agendaAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *agendaDescriptionLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topViewSpaceTopConstraint;

@end
