//
//  TBGradientView.h
//  shedappOBJC
//
//  Created by Talles  Borges  on 5/9/15.
//  Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface TBGradientView : UIView

@property (nonatomic) IBInspectable UIColor *colorStart;
@property (nonatomic) IBInspectable UIColor *colorEnd;

@end
