//
// Created by Talles  Borges  on 5/2/15.
// Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import <Foundation/Foundation.h>


static NSInteger const kEnterpriseId = 1;
static NSString *const kVersionEndpoint = @"/api/enterprises";

#define kEnterpriseEndpoint [NSString stringWithFormat:@"%@/%d",kVersionEndpoint,kEnterpriseId]

#define kFeedUrl [NSString stringWithFormat:@"%@/feed",kEnterpriseEndpoint,kEnterpriseId]
#define kInfoUrl [NSString stringWithFormat:@"%@/info",kEnterpriseEndpoint,kEnterpriseId]
#define kAgendaUrl [NSString stringWithFormat:@"%@/agenda",kEnterpriseEndpoint,kEnterpriseId]
#define kMasksUrl [NSString stringWithFormat:@"%@/masks",kEnterpriseEndpoint,kEnterpriseId]
#define kNotificationsUrl [NSString stringWithFormat:@"%@/notifications",kEnterpriseEndpoint,kEnterpriseId]