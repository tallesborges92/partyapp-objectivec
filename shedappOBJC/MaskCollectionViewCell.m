//
//  MaskCollectionViewCell.m
//  shedappOBJC
//
//  Created by Talles  Borges  on 2/4/15.
//  Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import "MaskCollectionViewCell.h"

@implementation MaskCollectionViewCell


- (void)awakeFromNib {
    [super awakeFromNib];

    self.layer.borderWidth = 2;
    self.layer.borderColor = [UIColor colorWithRed:0.69 green:0.67 blue:0.6 alpha:1].CGColor;

}


@end
