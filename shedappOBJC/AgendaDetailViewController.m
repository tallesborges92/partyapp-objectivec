//
// Created by Talles  Borges  on 5/9/15.
// Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import "AgendaDetailViewController.h"
#import "UIImageView+WebCache.h"
#import "YourClub.h"


@interface AgendaDetailViewController ()

@property CGRect defaultTopViewRect;

@end

@implementation AgendaDetailViewController {

}

- (void)viewDidLoad {
    [super viewDidLoad];

    NSDateFormatter *dfDate = [NSDateFormatter new];
    [dfDate setDateFormat:@"dd/MM/yyyy"];

    NSDateFormatter *dfHour = [NSDateFormatter new];
    [dfHour setDateFormat:@"HH:mm"];

    [_agendaImageView sd_setImageWithURL:[NSURL URLWithString:_agenda.eventImage] placeholderImage:[UIImage imageNamed:@"carregando"]];
    [_agendaDescriptionLabel setText:_agenda.eventLongDescription];
    [_agendaNameLabel setText:_agenda.eventName];
    [_agendaAddressLabel setText:[YourClub instance].enterprise.enterpriseAddress];

    [_agendaDateLabel setText:[dfDate stringFromDate:_agenda.eventStartDate]];
    [_agendaHourLabel setText:[dfHour stringFromDate:_agenda.eventStartDate]];

    UIImage *patternImg = [UIImage imageNamed:@"img_pattern"];
    self.view.backgroundColor = [UIColor colorWithPatternImage:patternImg];

    self.defaultTopViewRect = _agendaImageView.bounds;
}

- (IBAction)closePressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView.contentOffset.y < self.defaultTopViewRect.size.height) {
        float diff = -scrollView.contentOffset.y;
        float oldH = self.defaultTopViewRect.size.height;

        float newH = oldH + diff;

        self.topViewHeightConstraint.constant = newH;
        self.topViewSpaceTopConstraint.constant = scrollView.contentOffset.y;
    }
}

@end