//
//  AboutViewController.m
//  shedapp
//
//  Created by Talles  Borges  on 2/3/15.
//  Copyright (c) 2015 Talles Borges. All rights reserved.
//

#import <SDWebImage/UIImageView+WebCache.h>
#import "AboutViewController.h"
#import "Enterprise.h"
#import "Enterprise+RestKit.h"
#import "Constants.h"
#import "YourClub.h"

@interface AboutViewController ()

@property (nonatomic, assign) CGRect defaultTopViewRect;

@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.defaultTopViewRect = self.topView.frame;

    Enterprise *enterprise = [Enterprise enterpriseWithId:@(kEnterpriseId)];

    if (enterprise) {
        [self configureWithEnterprise:enterprise];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [Enterprise getWithSuccess:^(RKMappingResult *result) {
        NSLog(@"result = %@", result);
        [self configureWithEnterprise:[result firstObject]];
    } andFailure:^(NSError *error) {
        NSLog(@"error = %@", error);
    }];
}

#pragma mark - Util Methods

-(void) configureWithEnterprise:(Enterprise *)enterprise {
    [YourClub instance].enterprise = enterprise;

    [_enterpriseImageView sd_setImageWithURL:[NSURL URLWithString:enterprise.enterpriseImage]];
    [_logoImageView sd_setImageWithURL:[NSURL URLWithString:enterprise.enterpriseLogo]];

    [_nameLabel setText:enterprise.enterpriseName];
    [_telephoneLabel setText:enterprise.enterpriseTelephone];
    [_addressLabel setText:enterprise.enterpriseAddress];
    [_descriptionLabel setText:enterprise.enterpriseDescription];
}


#pragma mark - ScrollView Delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView.contentOffset.y < self.defaultTopViewRect.size.height) {
        float diff = -scrollView.contentOffset.y;
        float oldH = self.defaultTopViewRect.size.height;

        float newH = oldH + diff;

        self.topViewHeightConstraint.constant = newH;
        self.topViewSpaceTopConstraint.constant = scrollView.contentOffset.y;
    }
}

@end
