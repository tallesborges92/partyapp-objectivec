//
//  Enterprise.m
//  shedappOBJC
//
//  Created by Talles  Borges  on 5/5/15.
//  Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import "Enterprise.h"


@implementation Enterprise

@dynamic enterpriseName;
@dynamic enterpriseAddress;
@dynamic enterpriseTelephone;
@dynamic enterpriseDescription;
@dynamic enterpriseWebsite;
@dynamic enterpriseImage;
@dynamic enterpriseLatitude;
@dynamic enterpriseId;
@dynamic enterpriseLongitude;
@dynamic enterpriseLogo;
@dynamic enterpriseHashtag;

@end
