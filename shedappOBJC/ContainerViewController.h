//
//  ContainerViewController.h
//  shedapp
//
//  Created by Talles  Borges  on 2/3/15.
//  Copyright (c) 2015 Talles Borges. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString *const kYourClubNotification = @"kYourClubNotification";

@interface ContainerViewController : UIViewController

#pragma mark - Navitation Top
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

#pragma mark - Buttons
@property (weak, nonatomic) IBOutlet UIButton *feedButton;
@property (weak, nonatomic) IBOutlet UIButton *agendaButton;
@property (weak, nonatomic) IBOutlet UIButton *instaButton;
@property (weak, nonatomic) IBOutlet UIButton *infoButton;
@property (weak, nonatomic) IBOutlet UIButton *cameraButton;
@property (weak, nonatomic) IBOutlet UIButton *notificationButton;

@end
