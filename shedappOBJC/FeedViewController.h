//
//  FeedViewController.h
//  shedapp
//
//  Created by Talles  Borges  on 2/3/15.
//  Copyright (c) 2015 Talles Borges. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedViewController : UIViewController <UITableViewDataSource,UITabBarDelegate, NSFetchedResultsControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *feedTableView;

@property(nonatomic, strong) UIRefreshControl *refreshControl;
@end
