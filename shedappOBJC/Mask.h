//
//  Mask.h
//  shedappOBJC
//
//  Created by Talles  Borges  on 5/7/15.
//  Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Mask : NSManagedObject

@property (nonatomic, retain) NSNumber * maskId;
@property (nonatomic, retain) NSString * maskImage;
@property (nonatomic, retain) NSString * maskThumb;

@end
