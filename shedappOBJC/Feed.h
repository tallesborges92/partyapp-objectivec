//
//  Feed.h
//  shedappOBJC
//
//  Created by Talles  Borges  on 5/2/15.
//  Copyright (c) 2015 TallesBorges. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Feed : NSManagedObject

@property (nonatomic, retain) NSNumber * feedId;
@property (nonatomic, retain) NSString * feedDescription;
@property (nonatomic, retain) NSDate * feedDate;
@property (nonatomic, retain) NSString * feedImageUrl;

@end
